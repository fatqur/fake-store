export interface IRequestHeaders {
  accept: string;
  Authorization?: string;
  "Content-Type"?: string;
}
export interface ILoginParams {
  username: string;
  password: string;
}
export interface ILoginResponse {
  token: string;
}
export interface IUser {
  address: {
    geolocation: { lat: string; long: string };
    city: string;
    street: string;
    number: number;
    zipcode: string;
  };
  id: number;
  email: string;
  username: string;
  password: string;
  name: { firstname: string; lastname: string };
  phone: string;
}
