import { atom } from "recoil";

export type TToast = {
  id?: number;
  type: "ERROR" | "WARNING" | "SUCCESS";
  message: string;
};

export const toastArrayState = atom<TToast[] | []>({
  key: "toastArrayState",
  default: [],
});
