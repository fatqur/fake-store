import { atom } from "recoil";

interface UserState {
  id: number;
  avatar: string;
  email: string;
  name: string;
  role: string;
  code: string;
  phone: string;
}

type TLoginData = {
  avatar: string;
  id: number;
  name: string;
  email: string;
  gender: "MALE" | "FEMALE" | "UNKNOWN";
  dob: string;
  phone: string;
  app_token?: string | null;
  status: number;
  created_at: string;
  updated_at: string;
  deleted_at?: string | null;
  role: {
    id: number;
    name: string;
    created_at: string;
    updated_at: string;
    deleted_at?: string | null;
    permissions: { id: number; name: string }[];
  };
  outlets: {
    logo: string;
    id: number;
    name: string;
    code: string;
  }[];
};

interface GetSessionAdminData {
  avatar: string;
  code: string;
  id: number;
  name: string;
  email: string;
  password: string;
  gender: string;
  dob: Date;
  phone: string;
  app_token: string;
  status: number;
  created_at: Date;
  updated_at: Date;
  deleted_at: null;
  role: {
    id: number;
    name: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: null;
    permissions: {
      id: number;
      name: string;
    }[];
  };
  outlets: {
    logo: string;
    id: number;
    name: string;
    code: string;
  }[];
}

export const isLoginState = atom({
  key: "isLoginState",
  default: true,
});

export const permissionsState = atom<string[]>({
  key: "permissionsState",
  default: [],
});

export const userState = atom<UserState>({
  key: "userState",
  default: {
    id: 0,
    avatar: "",
    email: "",
    name: "",
    role: "",
    code: "",
    phone: "",
  },
});

export const outletState = atom<GetSessionAdminData["outlets"]>({
  key: "outletState",
  default: [],
});
