import axios from "axios";
import * as constants from "./constant";
import { IRequestHeaders } from "../types/global";

export const BASE_URL = constants.BASE_URL;

export const getTokenFromDevice = () => {
  return localStorage.getItem(constants.STORAGE_TOKEN_KEY);
};

export const API = (
  authenticated = true,
  token: string | null = null,
  isMultipart = false
) => {
  const requestHeaders: Partial<IRequestHeaders> = {
    accept: "application/json",
  };

  if (authenticated) {
    if (token) {
      requestHeaders.Authorization = "Bearer " + token;
    } else {
      const tokenOnDevice = getTokenFromDevice();
      if (tokenOnDevice !== null) {
        requestHeaders.Authorization = "Bearer " + tokenOnDevice;
      }
    }
  }

  if (isMultipart) {
    requestHeaders["Content-Type"] = "multipart/form-data";
  } else {
    requestHeaders["Content-Type"] = "application/json";
  }

  const cancelSource = axios.CancelToken.source();
  const request = axios.create({
    baseURL: BASE_URL,
    timeout: 60000, // 1 menute to cancel request
    headers: requestHeaders,
    cancelToken: cancelSource.token,
  });

  return { request, cancelSource };
};
