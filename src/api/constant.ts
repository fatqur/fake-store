export const BASE_URL = "https://fakestoreapi.com/";

export const STALE_SHORT = 5000; // 5 Secs
export const STALE_MEDIUM = 1000 * 60; // 1 Minute
export const STALE_LONG = 1000 * 3600; // 1 Hour

export const STORAGE_TOKEN_KEY = "TT";
export const TOKEN_RESET_PASSWORD = "_rp";
