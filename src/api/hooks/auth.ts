import { AxiosError } from "axios";
import { API } from "..";
import { ILoginParams, ILoginResponse } from "../../types/global";
import { useMutation } from "@tanstack/react-query";

export const useLogin = () => {
  const doLogin = async (params: ILoginParams) => {
    const { request } = API(false);
    const { data } = await request.post("auth/login", params);

    return data;
  };

  return useMutation<
    ILoginResponse,
    AxiosError<ILoginResponse, ILoginParams>,
    ILoginParams
  >(doLogin);
};

// export const useLogout = () => {
//   const setIsLogin = useSetRecoilState(isLoginState);
//   const navigate = useNavigate();

//   const doLogout = () => {
//     setIsLogin(false);
//     localStorage.removeItem(STORAGE_TOKEN_KEY);
//     navigate("/");
//   };

//   return doLogout;
// };

// const getSession = async (): Promise<
//   AxiosResponse<TResponseData<GetSessionAdminData>>
// > => {
//   const { request } = API();
//   const response: AxiosResponse<TResponseData<GetSessionAdminData>> =
//     await request.get("admin/me");

//   return response;
// };

// export const useGetSession = () => {
//   const navigate = useNavigate();
//   const setPermissions = useSetRecoilState(permissionsState);
//   const setUser = useSetRecoilState(userState);
//   const setOutlet = useSetRecoilState(outletState);
//   return useQuery({
//     queryKey: ["getSession"],
//     queryFn: () => getSession(),
//     onSuccess: (res) => {
//       if (res.status === 200) {
//         const { data } = res.data;

//         // outlets
//         setOutlet(data?.outlets ?? []);

//         // permission
//         const formatedPermissions = formatPermission([
//           ...data.role.permissions,
//           { id: 9999999, name: "profile" },
//           { id: 9999998, name: "forbidden" },
//           { id: 9999997, name: "dashboard.index" },
//         ]);
//         setPermissions(formatedPermissions);

//         // user
//         const user: IDataUser = {
//           id: data?.id as number,
//           avatar: data?.avatar as string,
//           email: data?.email as string,
//           role: data?.role?.name as string,
//           name: data?.name as string,
//           code: data?.code as string,
//           phone: data?.phone as string,
//         };
//         const dataUser = formatUser(user);
//         setUser(dataUser);
//       }
//     },
//     onError: (err: AxiosError) => {
//       if (err) {
//         errorHandling(err.response as any, navigate);
//       }
//     },
//   });
// };

// export const useRequestResetPassword = () => {
//   const doReset = async (body: IRequestResetPassword) => {
//     const { request } = API(false);
//     const { data } = await request.post("admin/reset-password", body);

//     return data;
//   };

//   return useMutation<
//     TResponseData<any>,
//     AxiosError<TResponseData<any>, IRequestResetPassword>,
//     IRequestResetPassword
//   >(doReset);
// };

// export const useResetPassword = () => {
//   const doReset = async (body: IResetPassword) => {
//     const { request } = API(false);
//     const { data } = await request.put("/admin/password", body);

//     return data;
//   };

//   return useMutation(doReset);
// };

// export const useValidationTokenReset = () => {
//   const doValidateToken = async (token: string) => {
//     const { request } = API(false);
//     const { data } = await request.post("/admin/reset-password/validate", {
//       token,
//     });

//     return data;
//   };

//   return useMutation(doValidateToken);
// };
