import { useQuery } from "@tanstack/react-query";
import { API } from "..";
import { IUser } from "../../types/global";

// useGetUsers
const getUsers = async () => {
  const { request } = API();
  const response = await request.get(`users`);

  return response.data as IUser[];
};

export const useGetUsers = () => {
  return useQuery({
    queryKey: ["getUsers"],
    queryFn: () => getUsers(),
    keepPreviousData: true,
  });
};

const getUserDetail = async (id: number) => {
  const { request } = API();
  const response = await request.get(`users/${id}`);

  return response.data as IUser;
};

export const useGetUserDetail = (id: number) => {
  return useQuery({
    queryKey: ["getUsersDetail"],
    queryFn: () => getUserDetail(id),
    keepPreviousData: true,
  });
};
