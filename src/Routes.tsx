import { Routes, Route } from "react-router-dom";
import Login from "./pages/Login";
import Users from "./pages/Users";
import UserDetail from "./pages/UserDetail";

export default function MyRoutes() {
  return (
    <Routes>
      <Route index element={<Login />} />
      <Route path="users">
        <Route index element={<Users />} />
        <Route path=":id" element={<UserDetail />} />
      </Route>
    </Routes>
  );
}
