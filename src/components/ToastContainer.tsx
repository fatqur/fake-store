import { useEffect } from "react";
import { useRecoilState } from "recoil";
import { toastArrayState } from "../stores";
import Toast from "./Toast";

export default function ToastContainer() {
  const [toasts, setToast] = useRecoilState(toastArrayState);

  useEffect(() => {
    let interval: ReturnType<typeof setInterval> | undefined;

    if (toasts.length) {
      interval = setInterval(() => {
        const newToast = [...toasts];
        newToast.shift();

        setToast([...newToast]);
      }, 2000);
    }

    return () => clearInterval(interval);
  });

  return (
    <div className="absolute top-[4.5rem] right-0 z-[9999] flex w-5/6 justify-end">
      <div className="max-w-xl">
        {toasts && toasts.map((toast, i) => <Toast key={i} {...toast} />)}
      </div>
    </div>
  );
}
