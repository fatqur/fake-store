import { useRecoilState } from "recoil";
import Close from "../assets/icons/Close";
import { toastArrayState } from "../stores";

type Props = {
  id?: number;
  type: string;
  message: string;
};

export default function Toast(props: Props) {
  const [toasts, setToast] = useRecoilState(toastArrayState);
  function getBackground(type: string) {
    if (type === "SUCCESS") {
      return "bg-green-100";
    } else if (type === "ERROR") {
      return "bg-red-100";
    } else if (type === "WARNING") {
      return "bg-yellow-100";
    }
  }

  function getColor(type: string) {
    if (type === "SUCCESS") {
      return "text-green-800";
    } else if (type === "ERROR") {
      return "text-red-800";
    } else if (type === "WARNING") {
      return "text-yellow-800";
    }
  }

  function getColorIconClose(type: string) {
    if (type === "SUCCESS") {
      return "#14AE5C";
    } else if (type === "ERROR") {
      return "#DA2D2D";
    } else if (type === "WARNING") {
      return "#D3A945";
    }
  }

  const handleDelete = () => {
    const newToast = [...toasts].filter((el) => el.id !== props.id);

    setToast([...newToast]);
  };

  return (
    <div
      className={`flex w-[546px] ${getBackground(
        props.type
      )} mb-4 items-center justify-between px-4 py-4 shadow`}
    >
      <div>
        <span className={`${getColor(props.type)}`}>{props.message}</span>
      </div>
      <button onClick={handleDelete}>
        <Close size={4} color={getColorIconClose(props.type)} />
      </button>
    </div>
  );
}
