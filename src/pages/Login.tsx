import { useNavigate } from "react-router-dom";
import loginImage from "../assets/img/login-image.jpg";
import { useLogin } from "../api/hooks";
import { useForm } from "react-hook-form";
import { ILoginParams } from "../types/global";
import { STORAGE_TOKEN_KEY } from "../api/constant";

export default function Login() {
  const navigate = useNavigate();


  const { mutate: doLogin } = useLogin();
  const {
    register,
    handleSubmit,
  } = useForm<ILoginParams>();

  // useCheckLogin();
  // const [isLogin, setIsLogin] = useRecoilState(isLoginState);
  // useEffect(() => {
  //   if (isLogin) {
  //     navigate("/users");
  //   }
  // }, [isLogin, navigate]);

  const handleLogin = async (data: ILoginParams) => {
    // setErrMessage("");
    try {
      doLogin(data, {
        onSuccess: (res) => {
          localStorage.setItem(STORAGE_TOKEN_KEY, res.token);
          console.log(res, "<<<<<<<<<<<,, res");
          // setIsLogin(true);
          navigate("/users");
        },
        // Error email / password
        onError: (err) => {
          alert(err.response?.data);
          console.log(err.response?.data, "<<<<<<<<<< err");
          // setErrMessage(err.response?.data.meta.message ?? "");
        },
      });
    } catch (error) {
      console.log(error, "<<< error login catch");
    }
  };
  return (
    <>
      <div className="flex min-h-full">
        <div className="flex flex-1 flex-col justify-center px-4 py-12 sm:px-6 lg:flex-none lg:px-20 xl:px-24">
          <div className="mx-auto w-full max-w-sm lg:w-96">
            <div>
              <h1 className="mt-6 text-4xl font-bold tracking-tight text-indigo-900">
                Fake Store
              </h1>
              <h2 className="mt-6 text-3xl font-bold tracking-tight text-gray-900">
                Sign in to your account
              </h2>
            </div>

            <div className="mt-8">
              <div className="mt-6">
                <form
                  className="space-y-6"
                  onSubmit={handleSubmit(handleLogin)}
                >
                  <div>
                    <label
                      htmlFor="username"
                      className="block text-sm font-medium leading-6 text-gray-900"
                    >
                      Username
                    </label>
                    <div className="mt-2">
                      <input
                        id="username"
                        type="username"
                        autoComplete="username"
                        required
                        className="block w-full rounded-md border-0 py-1.5 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        {...register("username", { required: true })}
                      />
                    </div>
                  </div>

                  <div className="space-y-1">
                    <label
                      htmlFor="password"
                      className="block text-sm font-medium leading-6 text-gray-900"
                    >
                      Password
                    </label>
                    <div className="mt-2">
                      <input
                        id="password"
                        type="password"
                        autoComplete="current-password"
                        required
                        className="block w-full rounded-md border-0 py-1.5 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        {...register("password", { required: true })}
                      />
                    </div>
                  </div>

                  <div>
                    <button
                      type="submit"
                      className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                    >
                      Sign in
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div className="relative hidden w-0 flex-1 lg:block">
          <img
            className="absolute inset-0 h-full w-full object-cover"
            src={loginImage}
            alt=""
          />
        </div>
      </div>
    </>
  );
}
