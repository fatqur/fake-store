import MyRoutes from "./Routes";
import ToastContainer from "./components/ToastContainer";

function App() {
  return (
    <>
      <ToastContainer />
      <MyRoutes />
    </>
  );
}

export default App;
