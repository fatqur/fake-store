import { useEffect } from "react";
import { useSetRecoilState } from "recoil";
import { STORAGE_TOKEN_KEY } from "../api/constant";
import { isLoginState } from "../stores";

export const useCheckLogin = () => {
  const setIsLogin = useSetRecoilState(isLoginState);
  useEffect(() => {
    if (localStorage.getItem(STORAGE_TOKEN_KEY)) {
      setIsLogin(true);
    } else {
      setIsLogin(false);
    }
  }, [setIsLogin]);
  // return { isLogin, setIsLogin };
};
